#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;
void DFS(int,int,int);
struct edge{ int v1;int v2;int weight;};
bool compare (edge& first,edge& second) { return (first.weight < second.weight); }

//// union-find set
int set_find(int* p,int x) {return x == p[x] ? x : (p[x] = set_find(p,p[x]));}
void set_union(int* p,int x, int y) {p[set_find(p,x)] = set_find(p,y);}

bool *adj = new bool[2500100001]; //50001 * 50001
int *edge_list = new int[50001]; // initialize in the abandon check
bool *kickout = new bool[50001]; // initialize in the big run
bool *visit = new bool[50001]; // initialize in the abandon check
int num_ver, num_edg; // refresh at big run
int last_edge; // use in DFS
vector<edge> vec; // initialize in the big run


int main(void){

int big_run;
cin >> big_run;
for(int big=0;big<big_run;++big){

    // initialize for each big run
    if(big>0)
        for(long long clear=0;clear<(num_ver+1)*(num_ver+1);++clear){
            adj[clear] = 0;
        }
    for(int clear=0;clear<50000;++clear){
        kickout[clear] = false;
    }
    vec.clear();
    cin >> num_ver >> num_edg;
    edge *a = new edge[num_edg];
    // initialize end

    for(int i=0;i<num_edg;++i){
        int v1,v2,weight;
        cin >> v1 >> v2 >> weight;
        a[i].v1 = v1;
        a[i].v2 = v2;
        a[i].weight = weight;
        vec.push_back(a[i]);
    }

    sort(vec.begin(),vec.begin()+num_edg,compare);

    // union-find set
    std::vector<int> kruskal; // the index of MST in vec
    std::vector<int> abandon; // the possible candidate for MST
    int *p = new int[50001];
    int last_input = 0;
    for (int i=0; i<num_ver; ++i) p[i] = i; // initialize the set
    
    for (int i=0,j=0; i < num_ver-1 && j < num_edg; ++i){
        // if cycle, find next
        while (set_find(p,vec[j].v1) == set_find(p,vec[j].v2)){
            // cout << "haha";
            abandon.push_back(j);
            j++;
        }
        // union the bridge
        set_union(p,vec[j].v1, vec[j].v2);
        kruskal.push_back(j); // record the MST index in vec
        last_input = j;
        j++;
    }

    for(int tie=last_input+1;true;++tie){
        if(vec[last_input].weight==vec[tie].weight){
            abandon.push_back(tie);
        }
        else break;
    }


    for(int kru=0;kru<kruskal.size();++kru){
        int v1,v2,dfs;
        dfs = kruskal[kru];
        v1 = vec[dfs].v1;
        v2 = vec[dfs].v2;
        adj[v1*(num_ver-1)+kru+1] = true;
        adj[v2*(num_ver-1)+kru+1] = true;
    }

    // printf("the incident matrix\n");
    // for(int row=1;row<num_ver+1;++row){
    //     for(int col=1;col<num_ver;++col){
    //         cout << adj[row*(num_ver-1)+col] << ' ';
    //     }
    //     cout << endl;
    // }

    // printf("we are abandon\n");
    // for (std::vector<int>::iterator i = abandon.begin(); i != abandon.end(); ++i)
    // {
    //     cout << *i << '\t';
    //     cout << vec[*i].v1 << '\t' << vec[*i].v2 << '\t' << vec[*i].weight << endl;
    // }
    // printf("\n\nTHE kruskal\n");
    // for (std::vector<int>::iterator i = kruskal.begin(); i != kruskal.end(); ++i)
    // {
    //     cout << *i << '\t';
    //     cout << vec[*i].v1 << '\t' << vec[*i].v2 << '\t' << vec[*i].weight << endl;

    // }


/////////// check abandon start !!!
    // printf("CHECK ABANDON START\n");
    for (std::vector<int>::iterator i = abandon.begin(); i != abandon.end(); ++i)
    {

        /// initialize
        last_edge = 0;
        for(int clear=0;clear<50001;++clear){
            edge_list[clear] = 0;
            visit[clear] = false;
        }

            // printf("HAHAHAHA\n");
            // cout << *i << '\t';
            // cout << vec[*i].v1 << '\t' << vec[*i].v2 << '\t' << vec[*i].weight << endl;
            // printf("HAHAHAHA\n");

        DFS(vec[*i].v1,vec[*i].v2,0); // DFS(start vertex,target vertex,zero)

        // printf("%d\n",edge_list[5] );
        int up = last_edge;
        int challenger = vec[*i].weight;
        // int cancel = 0;
        while(edge_list[up]){
            // printf("%lu\n",kruskal.size());
            // printf("%d\t%d\n",edge_list[up],vec[kruskal[edge_list[up]-1]].weight );
            int check = vec[kruskal[edge_list[up]-1]].weight;
            if(challenger<=check){
                kickout[edge_list[up]-1] = true;
                // printf("kickout num = %d\n",edge_list[up]-1 );
            }
            up = edge_list[up];
            // cancel++;
            // if(cancel >3) break;
        }
        // for the last edge
        // printf("%d\t%d\n",last_edge,vec[kruskal[last_edge-1]].weight );
        int check = vec[kruskal[last_edge-1]].weight;
        if(challenger<=check){
            kickout[last_edge-1] = true;
            // printf("kickout num = %d\n",last_edge-1 );
        }

    }

    int total_edge = 0;
    int total_weight = 0;
    for(int count=0;count<kruskal.size();++count){
        if(kruskal[count]>=0 && !kickout[count]){
        // if(kruskal[count]>=0 && false){
            ++total_edge;
            total_weight += vec[kruskal[count]].weight;
        }
    }

    //// print out the answer
    printf("%d %d\n",total_edge,total_weight);

    // release the memory allocation
    delete [] a;
    delete [] p;
    kruskal.clear();
    abandon.clear();

} // the end of big_run
    return 0;
} // the end of main function



void DFS(int cur_ver,int target,int parent_edge)
{
    // cout << cur_ver << '\t';
    visit[cur_ver] = true;    // the vertex is visited
    for (int cur_edg=1; cur_edg<num_ver; cur_edg++){ // cur_edg is column in incident matrix
        if (adj[cur_ver*(num_ver-1)+cur_edg]){
            for(int next=1;next<num_ver+1;++next){ // next is vertex and is row in incident matrix
                if(adj[next*(num_ver-1)+cur_edg] && !visit[next]){
                    if(next==target) last_edge = cur_edg; 
                    edge_list[cur_edg] = parent_edge;
                    // printf("DFS %d    %d\n",cur_edg,parent_edge );
                    // cout << cur_edg << '\t'<< parent_edge << endl;
                    DFS(next,target,cur_edg);
                }
            }
        }
    }
}





