#include <iostream>
using namespace std;

int main(void){
	int total_num_run;
	cin >> total_num_run;
	for(int run=0;run<total_num_run;++run){
		long long l,r;
		cin >> l >> r;
		long long lucky_num = 0,i;
		if(l<=777) i = 777; 
		else if(l%7==0) i = l;
		else i = l-(l%7)+7;

		for(;i<=r;i+=7){
			int num_7=0,num_4=0;
			long long check = i;
			while(check>0){
				if(check % 10 == 7) ++num_7;
				else if(check % 10 ==4) ++num_4;
				check /= 10;
			}
			if(num_7<3) continue; //not match the second constraint
			if(num_7<=num_4) continue; //not match the third constraint

			++lucky_num; //if check pass all three constraint
		}
		printf("%lld\n",lucky_num);

	}
	return 0;
}