#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

long long C[19][19][190][7] = {0}; //C[#7][#4][digit][residue]
// the digit of one in [digit] is the capital number, the first and the second digit are for the pow(10,XX)
// e.g. 30000 is [043], because 3 * pow(10,4)

void safe_case(int,int,int);
void seven_case(int);
void four_case(int,int);
int main(void){
    
    //////////// initial condition
    C[0][0][10][0] = 1; //0
    C[0][0][10][1] = 2; //1,8
    C[0][0][10][2] = 2; //2,9
    C[0][0][10][3] = 1; //3
    C[0][1][10][4] = 1; //4
    C[0][0][10][5] = 1; //5
    C[0][0][10][6] = 1; //6
    C[1][0][10][0] = 1; //7


    int residue = 0;
    for(int digit=1;digit<18;++digit){
        long long multi = pow(10,digit);
        // cout << "digit = " << digit << endl;
        // 0,original
        for(int first=0;first<=digit;++first){ //put the cumulative result into C
            for(int second=0;second<=digit;++second){
                    // printf("C%d%d\n",first,second ); // cout
                for(int fourth=0;fourth<7;++fourth){
                    C[first][second][digit*10][fourth] += C[first][second][(digit-1)*10+9][fourth];
                    // cout << C[first][second][(digit-1)*10+9][fourth] << '\t';
                }
                // cout << endl;
            }
        }
        // cout << endl;
        // 10,100,...
        residue = multi % 7;
        safe_case(1,digit,residue);
        // 20,200,...
        safe_case(2,digit,(residue*2)%7);
        // 30,300,...
        safe_case(3,digit,(residue*3)%7);
        // 40,400,...
        four_case(digit,(residue*4)%7);
        // 50,500,...
        safe_case(5,digit,(residue*5)%7);
        // 60,600,...
        safe_case(6,digit,(residue*6)%7);
        // 70,700,...
        seven_case(digit);
        // 80,800,...
        safe_case(8,digit,(residue*8)%7);
        // 90,900,...
        safe_case(9,digit,(residue*9)%7);
    
    }    
    
    for(int third=0;third<10;++third) ++C[0][0][third][0]; //0
    for(int third=1;third<10;++third) ++C[0][0][third][1]; //1
    for(int third=2;third<10;++third) ++C[0][0][third][2]; //2
    for(int third=3;third<10;++third) ++C[0][0][third][3]; //3
    for(int third=4;third<10;++third) ++C[0][1][third][4]; //4
    for(int third=5;third<10;++third) ++C[0][0][third][5]; //5
    for(int third=6;third<10;++third) ++C[0][0][third][6]; //6
    for(int third=7;third<10;++third) ++C[1][0][third][0]; //7
    for(int third=8;third<10;++third) ++C[0][0][third][1]; //8
    for(int third=9;third<10;++third) ++C[0][0][third][2]; //9
    ////////// programming end

    // for(int ddd=50;ddd<51;ddd+=10){
    //     long long Tsum = 0;
    //     for(int first=3;first<18;++first){ //put the cumulative result into C
    //         for(int second=0;second<18;++second){
    //             if(first-second>0){
    //                 Tsum += C[first][second][ddd][0];
    //                 if(C[first][second][ddd][0]>0) {
    //                     printf("C%d%d\n",first,second );
    //                     printf("%lld\n",Tsum);
    //                 }
    //             }
    //         }
    //     }
    // }

    /////////// input and check
    int num_input = 0;
    cin >> num_input;
    vector<int> v;
    v.clear();
    for(int run=0;run<num_input;++run){
        long long l,r,l2,r2;
        cin >> l >> r;
        ++l; ++r; //the boundary is included
        l2 = l; r2 = r;
        long long r_ans = 0;
        long long l_ans = 0;

        while(l2){
            v.push_back(l2%10);
            l2 /= 10;
        }
        int target;
        int resi;
        int interest = 0;
        long long front = 0;
        long long want = 0;
        for(int high=v.size()-1;high>=0;--high){
            target = v[high];
            if(target==7) ++interest;
            else if(target==4) --interest;
            resi = front % 7;
            for(int first=3;first<18;++first){
                for(int second=0;second<18;++second){
                    if(first-second>(-interest)){
                        want += C[first][second][high*10+target-1][(7-resi)%7];
                    }
                }
            }
            l_ans = want;
            front += pow(10,high) * target;
        }
        v.clear();
        want = 0;
        front = 0;
        interest = 0;
        while(r2){
            v.push_back(r2%10);
            r2 /= 10;
        }
        for(int high=v.size()-1;high>=0;--high){
            target = v[high];
            resi = front % 7;
            // cout << want << endl;
            // cout << high*10+target-1 << endl;
            if(target!=0){
            for(int first=3;first<18;++first){
                for(int second=0;second<18;++second){
                    if(first-second>(-interest)){
                        want += C[first][second][high*10+target-1][(7-resi)%7];
                        // if(C[first][second][high*10+target-1][(7-resi)%7]>0)    printf("C %d %d %d %d ",first,second,high*10+target-1,(7-resi)%7 );
                    }
                }
                // cout << "want "<<want << endl;
            }
        }
            // cout << want << endl;
            r_ans = want;
            front += pow(10,high) * target;
            // cout << front << endl;
            if(target==7) ++interest;
            else if(target==4) --interest;
        }
        v.clear();
        // printf("r_ans = %lld, l_ans = %lld\n",r_ans,l_ans);
        printf("%lld\n",r_ans - l_ans);
    }

    
    return 0;
}

void safe_case(int cap,int dig,int res){
    // cout << res << '\t';
    for(int first=0;first<=dig+1;++first){ //put the cumulative result into C
        for(int second=0;second<=dig+1;++second){
            // printf("C%d%d\t",first,second );

            for(int fourth=0;fourth<7;++fourth){
                C[first][second][dig*10+cap][fourth] = C[first][second][dig*10+cap-1][fourth];
            }
        }
    }
    for(int first=0;first<=dig;++first){ //put the new data into C
        for(int second=0;second<=dig;++second){
            for(int fourth=0;fourth<7;++fourth){
                C[first][second][dig*10+cap][(fourth+res)%7] += C[first][second][dig*10][fourth];
            }
        }
    }
}

void seven_case(int dig){
    for(int first=0;first<=dig+1;++first){ //put the cumulative result into C
        for(int second=0;second<=dig+1;++second){
            for(int fourth=0;fourth<7;++fourth){
                C[first][second][dig*10+7][fourth] = C[first][second][dig*10+6][fourth];
            }
        }
    }
    for(int first=0;first<=dig;++first){ //put the new data into C
        for(int second=0;second<=dig;++second){
            for(int fourth=0;fourth<7;++fourth){
                C[first+1][second][dig*10+7][(fourth)%7] += C[first][second][dig*10][fourth];
            }
        }
    }
}

void four_case(int dig,int res){
    // cout << "fourcall" << endl;
    for(int first=0;first<=dig+1;++first){ //put the cumulative result into C
        for(int second=0;second<=dig+1;++second){
            for(int fourth=0;fourth<7;++fourth){
                C[first][second][dig*10+4][fourth] = C[first][second][dig*10+3][fourth];
            }
        }
    }
    for(int first=0;first<=dig;++first){ //put the new data into C
        for(int second=0;second<=dig;++second){
                // printf("C%d%d\t",first,second );
            for(int fourth=0;fourth<7;++fourth){
                C[first][second+1][dig*10+4][(fourth+res)%7] += C[first][second][dig*10][fourth];
                // cout <<C[first][second][dig*10][fourth] << '\t' ;
                // cout <<C[first][second+1][dig*10+4][(fourth+res)%7] << '\t';
            }
            // cout << endl;
        }
    }
    // cout << endl;
}




