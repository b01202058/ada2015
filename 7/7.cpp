#include <iostream>
#include <algorithm>
using namespace std;


class Matrix{
public:
    int *arr;
    int num_node;
    Matrix(int n){
        num_node = n;
        arr = new int[n*n];
        for(int ele=0;ele<num_node*num_node;++ele) arr[ele] = 0;
    }
    void allo_edge(int e){
        int row,col;
        for(int run=0;run<e;++run){
            cin >> row >> col;
            arr[(row-1)*num_node + (col-1)]++;
            arr[(col-1)*num_node + (row-1)]++;
        }
    }
    void printout(){
        for(int row=0;row<num_node;++row){
            for(int col=0;col<num_node;++col){
                printf("%d\t",arr[row*num_node+col]);
            }
            printf("\n");
        }
    }
    void clear(){
        delete[] arr; 
    }
        
};

void check_identity(const Matrix& first,const Matrix& second){
    int num = first.num_node;
    int *per = new int[num];
    for(int one=0;one<num;++one) per[one] = one;

    while(true){
        bool jump = false;
        for(int row=0;row<num;++row){
            for(int col=0;col<num;++col){
                if(first.arr[row*num+col] != second.arr[per[row]*num+per[col]]){
                    jump = true;
                    break;
                }
            }
            if(jump) break;
        }
        if(!jump){
            for(int ele=0;ele<num;++ele)printf("%d ",per[ele]+1);
            printf("\n");
            delete[] per;
            return;
        }

        next_permutation(per,per+num);
    }
}

int main(void){
int big_run;
cin >> big_run;
for(int big=0;big<big_run;++big){

    int n_node,n_edge;
    cin >> n_node >> n_edge;
    Matrix G1(n_node);
    G1.allo_edge(n_edge);
    // G1.printout();
    Matrix G2(n_node);
    G2.allo_edge(n_edge);
    // G2.printout();

    check_identity(G1,G2);

    G1.clear();
    G2.clear();
}
    return 0;
}