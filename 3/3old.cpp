#include <iostream>
#include <list>
#include <algorithm>
#include <cmath>
using namespace std;

int my_mod(int,int);
int beat(int,int,int,int,int);
int main(void){

	list<int> l;
	list<int>::iterator it = l.begin();

	int n,c,e,p;
	int cha;
	int number_of_run;
	cin >> number_of_run;
	for(int run=0;run<number_of_run;run++){
		// n = 8;c = 5;e = 3;p = 37;
		cin >> n >> c >> e >> p;
		l.clear();
		l.push_back(1);


		// int test[7] = {3,8,6,2,5,4,1};
		for(cha=2;cha<=n;cha++){
			// cha = test[i];
			it = l.end();
			it--;
			while(1){  //the loop stop if challenger fail or challenger all the king.
				if(it==l.begin()){
					if(beat(cha,*it,c,e,p)){
						l.push_front(cha);
						break;
					}
				}
				if(beat(cha,*it,c,e,p)){
					--it;
				}
				else{
					it++;
					l.insert(it,cha);
					break;
				}
			}
		}
		for(it=l.begin();it!=l.end();++it){
			cout << *it << ' ';
		}
		cout << endl;
		l.clear();
	}

	return 0;
}


int beat(int f,int k,int c,int e,int p){ //k means "king", f means "challenger"
	long long current = c*(f-k)*pow((f+k),e);
	long long result;
	if(current>0) result = current % p;
	else result = current % p + p;
	// cout << "result is " << result << endl;
	if(result > p/2) return 1; //challenger beat the king
	else return 0; //king keep his position
}
int my_mod(int a,int m){
	if(a>0) return a%m;
	else return a%m+m;
}