#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

int beat(int,int,int,long long,long long,vector<int> &);
int main(void){

	list<int> l;
	list<int>::iterator it = l.begin();
	vector<int> v; //to transform e into two base

	int n,c;
	long long e,p;
	int cha;
	int number_of_run;
	cin >> number_of_run;
	for(int run=0;run<number_of_run;run++){
		cin >> n >> c >> e >> p;
		l.clear();
		l.push_back(1);

		long long e_link = e;
		while(e_link!=0){
			v.push_back(e_link % 2);
			e_link /= 2;
		}

		for(cha=2;cha<=n;cha++){
			it = l.end();
			it--;
			while(1){  //the loop stop if challenger fail or challenger all the king.
				if(it==l.begin()){
					if(beat(cha,*it,c,e,p,v)){
						l.push_front(cha);
						break;
					}
				}
				if(beat(cha,*it,c,e,p,v)){
					--it;
				}
				else{
					it++;
					l.insert(it,cha);
					break;
				}
			}

		}
		for(it=l.begin();it!=l.end();++it){
			cout << *it << ' ';
		}
		cout << endl;
		l.clear();
		v.clear();
	}
	return 0;
}

int beat(int f,int k,int c,long long e,long long p,vector<int> &v3){ //k means "king", f means "challenger"
	long long z; // z is the coefficient which is not related to power
	if(f-k > 0) z = c*(f-k) % p;
	else z = c*(f-k) % p + p;
	unsigned long long q = f+k;
	unsigned long long big_r = 1;
	vector<unsigned long long> base;
	base.push_back(q);
	for(int digit=1;digit<v3.size();digit++){
		base.push_back((base[digit-1]*base[digit-1])%p);
	}
	for(int digit=0;digit<v3.size();digit++){
		for(int each=0;each<v3[digit];each++){
			big_r *= base[digit];
			if(big_r>p) big_r = big_r % p; // to avoid big_r overflowing		
		}
	}
	unsigned long long current = z*big_r; // both z and big_r are smaller than p, so "current" will not overflow
	unsigned long long result = current % p;
	base.clear();
	if(result > p/2) return 1; //challenger beat the king
	else return 0; //king keep his position
}
