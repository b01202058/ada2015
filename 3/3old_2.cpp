#include <iostream>
#include <list>
#include <algorithm>
#include <cmath>
using namespace std;

int my_mod(int,int);
int beat(int,int,int,int,int);
int main(void){

	list<int> l;
	list<int>::iterator it = l.begin();

	int n,c,p;
	long long e;
	int cha;
	int number_of_run;
	cin >> number_of_run;
	for(int run=0;run<number_of_run;run++){
		// n = 8;c = 5;e = 3;p = 37;
		cin >> n >> c >> e >> p;
		l.clear();
		l.push_back(1);

		// int test[7] = {3,8,6,2,5,4,1};
		for(cha=2;cha<=n;cha++){
			// cha = test[i];
			it = l.end();
			it--;
			while(1){  //the loop stop if challenger fail or challenger all the king.
				if(it==l.begin()){
					if(beat(cha,*it,c,e,p)){
						l.push_front(cha);
						break;
					}
				}
				if(beat(cha,*it,c,e,p)){
					--it;
				}
				else{
					it++;
					l.insert(it,cha);
					break;
				}
			}
		}
		for(it=l.begin();it!=l.end();++it){
			cout << *it << ' ';
		}
		cout << endl;
		l.clear();
	}
	return 0;
}

int beat(int f,int k,int c,int e,int p){ //k means "king", f means "challenger"
	long long z; // z is the coefficient which is not related to power
	if(f-k > 0) z = c*(f-k) % p;
	else z = c*(f-k) % p + p;


	long long rj; // rj represents the remainder congruence to q^j
	long long big_r = 1;
	while(e>0){
		long long q = f+k;
		long long x = 1; // x represent current power of loop
		for(int j=0;j<floor(log10(e)/log10(3));j++){
			rj = (q*q*q)%p;
			q = rj;
			x *= 3;
		}
		big_r *= q;
		if(big_r>p) big_r = big_r % p; // to avoid big_r overflowing
		e -= x;
	}


	long long current = z*big_r; // both z and big_r are smaller than p, so "current" will not overflow
	long long result = current % p;
	// cout << "result is " << result << endl;
	if(result > p/2) return 1; //challenger beat the king
	else return 0; //king keep his position
}
int my_mod(int a,int m){
	if(a>0) return a%m;
	else return a%m+m;
}