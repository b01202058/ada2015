#include <iostream>
#include <list>
#include <vector>
using namespace std;

int n;
long long c,e,p;
int beat(int,int);
// long long power_save[400000]={1};
int main(void){

    list<int> l;
    list<int>::iterator it = l.begin();

    int cha;
    int number_of_run;
    cin >> number_of_run;
    for(int run=0;run<number_of_run;++run){
        cin >> n >> c >> e >> p;
        l.clear();
        l.push_back(1);

        // long long e_2 = e;
        // long long q;
        // long long big_r = 1;
        // for(int bbb=2;bbb<2*n;++bbb){
        //     q = bbb;
        //     while (e_2){
        //         if(e_2 & 0x1){
        //             big_r *= q; // if e is not zero
        //             big_r %= p;}
        //         q *= q;
        //         e_2 >>= 1;
        //         q %= p;
        //     }
        //     power_save[bbb] = big_r;
        // }
        for(cha=2;cha<=n;++cha){
            it = l.end();
            it--;
            while(1){  //the loop stop if challenger fail or challenger beat all the king.
                if(it==l.begin()){
                    if(beat(cha,*it)){
                        l.push_front(cha);
                        break;
                    }
                }
                long long z; // z is the coefficient which is not related to power
                if(cha-*it > 0) z = c*(cha-*it) % p;
                else z = (c*(cha-*it)) % p + p;
                // if(cha==200000) cout << cha <<  '\t' << *it << '\t' << z << endl;
                long long q = cha+*it;
                long long big_r = 1;
                long long e_2 = e;
                while (e_2){
                    if(e_2 & 0x1){
                        big_r *= q; // if e is not zero
                        big_r %= p;}
                    q *= q;
                    e_2 >>= 1;
                    q %= p;
                }
                // int in = cha+*it;
                q = z*big_r; // both z and big_r are smaller than p, so "current" will not overflow
                q %= p;
                if(q > p/2) --it; //challenger beat the king
                else{
                    it++;
                    l.insert(it,cha);
                    break;
                }

                // if(beat(cha,*it)){
                    // --it;
                // }
                // else{
                //  it++;
                //  l.insert(it,cha);
                //  break;
                // }
            }

        }
        for(it=l.begin();it!=l.end();++it){
            // cout << *it << ' ';
            printf("%d ",*it);
        }
        cout << endl;
    }
    return 0;
}

int beat(int f,int k){ //k means "king", f means "challenger"
    long long z; // z is the coefficient which is not related to power
    if(f-k > 0) z = c*(f-k) % p;
    else z = (c*(f-k)) % p + p;
    // if(f==200000) cout << f <<  '\t' << k << '\t' << z << endl;
    long long q = f+k;
    long long big_r = 1;
    long long e_2 = e;
    while (e_2){
        if(e_2 & 0x1){
            big_r *= q; // if e is not zero
            big_r %= p;}
        q *= q;
        e_2 >>= 1;
        q %= p;
    }

    q = z*big_r; // both z and big_r are smaller than p, so "current" will not overflow
    q %= p;
    if(q > p/2) return 1; //challenger beat the king
    else return 0; //king keep his position
}
